package CNode;

import GUI.AnimationController;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class CNode extends Rectangle {

    private int value;

    public CNode(int n) {
        value = n;
        setY(65);

    }

    public void setValue(int input) {
        value = input;
    }
    
    public int getValue() {
        return value;
    }

    public TranslateTransition moveX(int x) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setByX(x);

        return t;
    }

    public TranslateTransition moveY(int y) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setByY(y);

        return t;
    }
}
