/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CNode;

import GUI.AnimationController;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 *
 * @author Computer
 */
public class TNode extends Text {

    public TNode(String input) {
        setFill(Color.web("#1e2023"));
        setFont(Font.font("Calibri", 20));
        setY(120);

        setText(input);
    }


    public int getInt() {
        return Integer.parseInt(this.getText());
    }

    public TranslateTransition moveX(int x) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setByX(x);

        return t;
    }

    public TranslateTransition moveX(int x, int time) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(time));
        t.setByX(x);

        return t;
    }

    public TranslateTransition moveY(int y) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setByY(y);

        return t;
    }

    public TranslateTransition moveY(int y, int time) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(time));
        t.setByY(y);

        return t;
    }

    public TranslateTransition movetoX(int x) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setToX(x);

        return t;
    }
    
    public TranslateTransition movetoXY(int x,int y) {
        TranslateTransition t = new TranslateTransition();
        t.setNode(this);
        t.setDuration(Duration.millis(AnimationController.DELAY));
        t.setToX(x-getX());
        t.setToY(y-getY());
        return t;
    }
}
