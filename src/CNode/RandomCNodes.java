package CNode;

import java.util.Random;

import GUI.AnimationController;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class RandomCNodes {

    public static CNode[] randomCNodes(int n) {
        CNode[] arr = new CNode[n];
        int CNODE_WIDTH = ((AnimationController.WINDOW_WIDTH - 200) / arr.length);

        Random r = new Random();

        for (int i = 0; i < arr.length; i++) {

            arr[i] = new CNode(1 + r.nextInt(99));
            arr[i].setX(i * CNODE_WIDTH);
            arr[i].setFill(Color.web("#ff428e"));
            setCNodeDim(arr[i], arr.length);
        }
        return arr;

    }

    public static TNode[] getTnodes(CNode[] arr) {

        TNode[] text_arr = new TNode[arr.length];

        for (int i = 0; i < arr.length; i++) {

            text_arr[i] = new TNode(Integer.toString(arr[i].getValue()));
            if (arr[i].getValue() < 10) {
                text_arr[i].setX(arr[i].getX() + 29);
            } else {
                text_arr[i].setX(arr[i].getX() + 25);
            }

        }
        return text_arr;
    }

    public static TNode[] setIndex(CNode[] arr) {

        TNode[] text_arr = new TNode[arr.length];

        for (int i = 0; i < arr.length; i++) {

            text_arr[i] = new TNode(Integer.toString(i));
            text_arr[i].setFill(Color.WHITE);
            text_arr[i].setY(40);
            text_arr[i].setX(arr[i].getX() + 30);
            text_arr[i].setFont(Font.font("Calibri", 15));

        }
        return text_arr;
    }
    
    

    public static TNode[] setDescription(int n) {

        TNode[] text_arr = new TNode[n];

        for (int i = 0; i < n; i++) {

            text_arr[i] = new TNode(" ");
            text_arr[i].setFill(Color.web("#1e2023"));
            text_arr[i].setFont(Font.font("Calibri", 30));

        }
        return text_arr;
    }

    private static void setCNodeDim(CNode cnode, int n) {
        cnode.setWidth((AnimationController.WINDOW_WIDTH - 200) / n - AnimationController.XGAP);
        cnode.setHeight(100);
    }
}
