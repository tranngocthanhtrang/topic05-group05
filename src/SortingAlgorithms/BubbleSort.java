package SortingAlgorithms;

import CNode.CNode;
import CNode.TNode;
import GUI.AnimationController;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.animation.PauseTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class BubbleSort extends AbstractSort {

    private boolean swapped;
    private final ArrayList<Transition> transitions;

    public BubbleSort() {
        this.transitions = new ArrayList<>();
    }

    private void compareCNode(CNode[] arr, TNode[] text_arr, int a, int b, TNode d) {

        transitions.add(colorCNode(arr, SELECT_COLOR, a, b));

        if (arr[a].getValue() > arr[b].getValue()) {

            transitions.add(colorTNode(d, WHITE_COLOR));
            transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
            transitions.add(colorTNode(d, BLACK_COLOR));
            transitions.add(swap(arr, text_arr, a, b));
            swapped = true;
        }

        transitions.add(colorCNode(arr, START_COLOR, a, b));
    }

    private void bubbleSort(CNode[] arr, TNode[] text_arr, TNode d) {

        for (int i = 0; i < arr.length; i++) {
            swapped = false;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                compareCNode(arr, text_arr, j, j + 1, d);
            }
            transitions.add(colorCNode(Arrays.asList(arr[arr.length - 1 - i]), SORTED_COLOR));

            if (!swapped) {
                break;
            }
        }

    }

    @Override
    public ArrayList<Transition> startSort(CNode[] arr, TNode[] text_arr, TNode[] d) {
        transitions.clear();
        d[0].setText("a[i] < a[i+1]. Let's Swap!");
        d[0].setX((AnimationController.WINDOW_WIDTH-200-d[0].getBoundsInLocal().getWidth())/2);
        d[0].setY(280);
        bubbleSort(arr, text_arr, d[0]);
        transitions.add(colorCNode(Arrays.asList(arr), SORTED_COLOR));
        return transitions;

    }

}
