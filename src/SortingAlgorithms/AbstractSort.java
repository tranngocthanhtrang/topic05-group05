package SortingAlgorithms;

import java.util.ArrayList;
import java.util.List;

import CNode.CNode;
import CNode.TNode;
import GUI.AnimationController;
import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public abstract class AbstractSort {

    final Color START_COLOR = Color.web("#ff428e");
    final Color SELECT_COLOR = Color.web("#fff2fd");
    
    final Color SORTED_COLOR = Color.web("#37ff8e");
    
    final Color WHITE_COLOR = Color.web("#ffffff");
    final Color BLACK_COLOR = Color.web("#1e2023");

    static int DX = (AnimationController.WINDOW_WIDTH - 200) / AnimationController.NO_OF_CNODES;

    static int DY = 100;
    

    ParallelTransition colorCNode(CNode[] arr, Color color, int... a) {
        ParallelTransition pt = new ParallelTransition();

        for (int i = 0; i < a.length; i++) {
            FillTransition ft = new FillTransition();
            ft.setShape(arr[a[i]]);
            ft.setToValue(color);
            ft.setDuration(Duration.millis(AnimationController.DELAY));

            pt.getChildren().add(ft);
        }
        return pt;
    }

    ParallelTransition colorOneCNode(CNode arr, Color color) {
        ParallelTransition pt = new ParallelTransition();

        FillTransition ft = new FillTransition();
        ft.setShape(arr);
        ft.setToValue(color);
        ft.setDuration(Duration.millis(AnimationController.DELAY));
        pt.getChildren().add(ft);

        return pt;
    }

    FillTransition showTNode(TNode t, boolean show) {

        FillTransition ft = new FillTransition();
        ft.setShape(t);

        if (!show) {
            ft.setToValue(Color.web("#1e2023"));
            ft.setDuration(Duration.millis(AnimationController.DELAY));

        } else {

            ft.setToValue(Color.WHITE);
            ft.setDuration(Duration.millis(AnimationController.DELAY));

        }

        return ft;

    }

    ParallelTransition colorTNode(TNode text_node, Color color) {
        ParallelTransition pt = new ParallelTransition();

        FillTransition ft = new FillTransition();
        ft.setShape(text_node);
        ft.setToValue(color);
        ft.setDuration(Duration.millis(AnimationController.DELAY));
        pt.getChildren().add(ft);
        return pt;
    }

    ParallelTransition colorCNode(CNode[] arr, Color color1, Color color2, int i, int j) {
        ParallelTransition pt = new ParallelTransition();

        FillTransition ft = new FillTransition();
        ft.setShape(arr[i]);
        ft.setToValue(color1);
        ft.setDuration(Duration.millis(AnimationController.DELAY));
        pt.getChildren().add(ft);

        FillTransition ft1 = new FillTransition();
        ft1.setShape(arr[j]);
        ft1.setToValue(color2);
        ft1.setDuration(Duration.millis(AnimationController.DELAY));
        pt.getChildren().add(ft1);

        return pt;
    }

    ParallelTransition colorCNode(List<CNode> list, Color color) {
        ParallelTransition pt = new ParallelTransition();

        for (CNode c : list) {
            FillTransition ft = new FillTransition();
            ft.setShape(c);
            ft.setToValue(color);
            ft.setDuration(Duration.millis(AnimationController.DELAY));
            pt.getChildren().add(ft);
        }

        return pt;
    }

    ParallelTransition colorCNode(CNode[] list, int a, int b, Color color) {
        ParallelTransition pt = new ParallelTransition();
        int i;

        for (i = a; i <= b; i++) {
            FillTransition ft = new FillTransition();
            ft.setShape(list[i]);
            ft.setToValue(color);
            ft.setDuration(Duration.millis(AnimationController.DELAY));
            pt.getChildren().add(ft);
        }

        return pt;
    }

    ParallelTransition colorTNode(TNode[] arr, Color color, int... a) {
        ParallelTransition pt = new ParallelTransition();

        for (int i = 0; i < a.length; i++) {
            FillTransition ft = new FillTransition();
            ft.setShape(arr[a[i]]);
            ft.setToValue(color);
            ft.setDuration(Duration.millis(AnimationController.DELAY));
            pt.getChildren().add(ft);
        }
        return pt;
    }

    ParallelTransition takeout(CNode[] arr, TNode[] text_arr, int... a) {
        ParallelTransition pt = new ParallelTransition();
        int i;
        for (i = 0; i < a.length; i++) {
            pt.getChildren().addAll(arr[a[i]].moveY(DY), text_arr[a[i]].moveY(DY));
        }

        return pt;
    }

    ParallelTransition takeback(CNode[] arr, TNode[] text_arr, int... a) {
        ParallelTransition pt = new ParallelTransition();
        int i;
        for (i = 0; i < a.length; i++) {
            pt.getChildren().addAll(arr[a[i]].moveY(-DY), text_arr[a[i]].moveY(-DY));
        }

        return pt;
    }

    ParallelTransition swap(CNode[] arr, TNode[] text_arr, int i, int j) {
        ParallelTransition pt = new ParallelTransition();
        int dxFactor = j - i;
        pt.getChildren().addAll(arr[i].moveX(DX * dxFactor), arr[j].moveX(-DX * dxFactor), text_arr[i].moveX(DX * dxFactor), text_arr[j].moveX(-DX * dxFactor));
        CNode tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;

        TNode t_tmp = text_arr[i];
        text_arr[i] = text_arr[j];
        text_arr[j] = t_tmp;
        return pt;
    }

    public abstract ArrayList<Transition> startSort(CNode[] arr, TNode[] text_arr, TNode[] d);
}
