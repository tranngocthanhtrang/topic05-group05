package SortingAlgorithms;

import CNode.CNode;
import CNode.TNode;
import GUI.AnimationController;
import static SortingAlgorithms.AbstractSort.DY;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.animation.PauseTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class RadixSort extends AbstractSort {

    private ArrayList<Transition> transitions;
    private static final Color MAX_COLOR = Color.AQUAMARINE;

    public RadixSort() {
        this.transitions = new ArrayList<>();
    }

    private void showDescription(TNode t) {
        transitions.add(t.moveY(-DY, 1));
        transitions.add(showTNode(t, true));
        transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
        transitions.add(showTNode(t, false));
        transitions.add(t.moveY(DY, 1));
    }

    public int getMax(CNode[] arr, TNode[] text_arr, TNode[] d) {
        showDescription(d[0]);
        int mx = arr[0].getValue();
        int max_index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].getValue() > mx) {
                mx = arr[i].getValue();
                max_index = i;
            }
        }

        transitions.add(colorCNode(arr, MAX_COLOR, max_index));
        transitions.add(colorCNode(arr, START_COLOR, max_index));
        transitions.add(colorCNode(arr, MAX_COLOR, max_index));
        transitions.add(colorCNode(arr, START_COLOR, max_index));
        return mx;
    }

    public int find_text(int num, TNode[] t, int[] visited) {
        int i = 0;
        for (i = 0; i < 10; i++) {

            if (visited[i] == 0 && t[i].getInt() == num) {
                visited[i] = 1;
                return i;
            }
        }
        return -1;
    }


    public void countSort(int[] num_arr, TNode[] arr, int exp, TNode[] d) {
        int n = arr.length;
        int output[] = new int[n];
        int i;
        int dest;
        int mang[][] = new int[10][10];
        int count[] = new int[10];
        Arrays.fill(count, 0);
        int visited[] = new int[10];

        for (i = 0; i < n; i++) {
            int basket = (num_arr[i] / exp) % 10;
            mang[basket][count[basket]] = num_arr[i];
            count[basket]++;
        }

        int[] count_ori = new int[10];

        showDescription(d[3]);
        for (i = 0; i < n; i++) {
            dest = find_text(num_arr[i], arr, visited);
            int basket = (arr[dest].getInt() / exp) % 10;
            transitions.add(colorTNode(arr[dest], SELECT_COLOR));
            int toY = 200 + count_ori[basket] * 20;
            if (arr[dest].getInt() < 10) {
                transitions.add(arr[dest].movetoXY(DX * basket + 29, toY));
            } else {
                transitions.add(arr[dest].movetoXY(DX * basket + 25, toY));
            }
            count_ori[basket]++;
        }

        for (i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }

        Arrays.fill(visited, 0);
        // Build the output array 
        for (i = n - 1; i >= 0; i--) {
            int basket = (num_arr[i] / exp) % 10;
            output[count[basket] - 1] = num_arr[i];
            count[basket]--;
        }

        for (i = 0; i < n; i++) {
            System.out.print(output[i] + " ");
        }
        System.out.println();

        for (i = 0; i < n; i++) {
            num_arr[i] = output[i];
        }

        int up = 0;
        showDescription(d[4]);
        for (i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (mang[i][j] != 0) {
                    dest = find_text(mang[i][j], arr, visited);

                    if (arr[dest].getInt() < 10) {
                        transitions.add(arr[dest].movetoXY(DX * up + 29, 120));
                    } else {
                        transitions.add(arr[dest].movetoXY(DX * up + 25, 120));
                    }
                    transitions.add(colorTNode(arr[dest], BLACK_COLOR));
                    up++;
                }

            }

        }
    }

    public void radixsort(CNode[] arr, TNode[] text_arr, TNode[] d) {
        // Find the maximum number to know number of digits 

        int m = getMax(arr, text_arr, d);

        // Do counting sort for every digit. Note that instead 
        // of passing digit number, exp is passed. exp is 10^i 
        // where i is current digit number 
        int[] num_arr = new int[10];
        for (int i = 0; i < 10; i++) {
            num_arr[i] = text_arr[i].getInt();
        }
        for (int exp = 1; m / exp > 0; exp *= 10) {
            if (exp == 1){
                showDescription(d[1]);
            }
            else{
                showDescription(d[2]);
            }
            countSort(num_arr, text_arr, exp, d);
        }
    }

    @Override
    public ArrayList<Transition> startSort(CNode[] arr, TNode[] text_arr, TNode[] d) {
        transitions.clear();

        radixsort(arr, text_arr, d);

        d[0].setText("Find the maximum number to know number of digits ");
        d[1].setText("Sorting by least significant digit");
        d[2].setText("Sorting by most significant digit");
        d[3].setText("Do counting sort for every digit");
        d[4].setText("Build the output array ");
        for (int i = 0; i <= 4; i++) {
            d[i].setX((AnimationController.WINDOW_WIDTH - 200 - d[i].getBoundsInLocal().getWidth()) / 2);
            d[i].setY(420);
        }

        transitions.add(colorCNode(Arrays.asList(arr), SORTED_COLOR));
        return transitions;
    }
}
