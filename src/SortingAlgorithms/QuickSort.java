package SortingAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import CNode.CNode;
import CNode.TNode;
import GUI.AnimationController;
import javafx.animation.PauseTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class QuickSort extends AbstractSort {

    private static final Color PIVOT_COLOR = Color.DARKMAGENTA;
    private static final Color I_COLOR = Color.MEDIUMTURQUOISE;
    private ArrayList<Transition> transitions;

    public QuickSort() {
        this.transitions = new ArrayList<>();
    }

    private void quickSort(CNode[] arr, TNode[] text_arr, int lo, int hi, TNode[] d) {
        if (lo < hi) {
            int q = partition(arr, text_arr, lo, hi, d);
            quickSort(arr, text_arr, lo, q - 1, d);
            quickSort(arr, text_arr, q + 1, hi, d);
        }
        
    }

    private void showDescription(TNode t) {
        transitions.add(t.moveY(-DY, 1));
        transitions.add(showTNode(t, true));
        transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
        transitions.add(showTNode(t, false));
        transitions.add(t.moveY(DY, 1));
    }

    // last elt of array chosen as pivot
    private int partition(CNode[] arr, TNode[] text_arr, int lo, int hi, TNode[] d) {
        int i = lo - 1;

        transitions.add(colorCNode(arr, lo, hi, SELECT_COLOR));
        transitions.add(colorCNode(arr, lo, hi, START_COLOR));
        transitions.add(colorCNode(arr, lo, hi, SELECT_COLOR));
        transitions.add(colorCNode(arr, lo, hi, START_COLOR));
        showDescription(d[5]);

        transitions.add(d[6].movetoX((int) text_arr[lo].getX() - DX));
        transitions.add(d[7].movetoX((int) text_arr[lo].getX()));

        transitions.add(colorCNode(arr, PIVOT_COLOR, hi));
        showDescription(d[0]);
        transitions.add(takeout(arr, text_arr, hi));

        for (int j = lo; j < hi; j++) {
            transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
            if (j!=lo){
            transitions.add(d[7].moveX(DX));
            }
            transitions.add(takeout(arr, text_arr, j));
            transitions.add(colorCNode(arr, SELECT_COLOR, j, hi));
            transitions.add(colorCNode(arr, START_COLOR, PIVOT_COLOR, j, hi));
            
            if (arr[j].getValue() < arr[hi].getValue()) {
                showDescription(d[1]);
                i++;
                transitions.add(d[6].moveX(DX));
                transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
                showDescription(d[2]);

                if (i != j) {
                    transitions.add(takeout(arr, text_arr, i));
                }
                transitions.add(swap(arr, text_arr, i, j));
                transitions.add(takeback(arr, text_arr, i, j));

            } else {
                transitions.add(takeback(arr, text_arr, j));
            }
        }
        showDescription(d[3]);
        transitions.add(swap(arr, text_arr, i + 1, hi));
        transitions.add(takeback(arr, text_arr, i + 1));
        transitions.add(colorCNode(arr, START_COLOR, i + 1));

        showDescription(d[4]);
        transitions.add(colorCNode(arr, I_COLOR, i + 1));
        transitions.add(colorCNode(arr, START_COLOR, i + 1));
        return i + 1;
    }

    @Override
    public ArrayList<Transition> startSort(CNode[] arr, TNode[] text_arr, TNode[] d) {

        transitions.clear();
        d[0].setText("Choose the last element as pivot.");
        d[1].setText("a[j] < pivot -> i++");
        d[2].setText("swap a[i],a[j]");
        d[3].setText("swap a[i+1],pivot");
        d[4].setText("return i+1");
        d[5].setText("i = low - 1");
        d[6].setText("i");
        d[6].setX(0);
        d[7].setText("j");
        d[7].setX(8);

        for (int i = 0; i <= 7; i++) {
            if (i <= 5) {
                d[i].setX((AnimationController.WINDOW_WIDTH-200-d[i].getBoundsInLocal().getWidth())/2);
                d[i].setY(420);
            } else {
                d[i].setFont(Font.font("Calibri", 20));
                d[i].setFill(Color.WHITE);
                d[i].setY(190);

            }

        }

        quickSort(arr, text_arr, 0, arr.length - 1, d);
        transitions.add(colorCNode(Arrays.asList(arr), SORTED_COLOR));

        return transitions;
    }

}
