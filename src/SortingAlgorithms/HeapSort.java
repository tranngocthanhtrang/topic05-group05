package SortingAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import CNode.CNode;
import CNode.TNode;
import GUI.AnimationController;
import static SortingAlgorithms.AbstractSort.DY;
import javafx.animation.PauseTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class HeapSort extends AbstractSort {

    private static final Color ROOT_COLOR = Color.GOLD;
//    private static final Color H_COLOR = Color.AQUAMARINE;
    private ArrayList<Transition> transitions;

    public HeapSort() {
        this.transitions = new ArrayList<>();
    }

    private void showDescription(TNode t) {
        transitions.add(t.moveY(-DY, 1));
        transitions.add(showTNode(t, true));
        transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
        transitions.add(showTNode(t, false));
        transitions.add(t.moveY(DY, 1));
    }

    private void heapify(CNode[] arr, TNode[] text_arr, int i, int n, TNode[] d) {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int max = i;
               if (left < n && right < n) {
            showDescription(d[0]);
            transitions.add(takeout(arr, text_arr, left, right, i));
            transitions.add(colorCNode(arr, ROOT_COLOR, i));
            transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
            transitions.add(takeback(arr, text_arr, left, right, i));
        }
        else if (i == 4){
            showDescription(d[0]);
            transitions.add(takeout(arr, text_arr,4,9));
            transitions.add(colorCNode(arr, ROOT_COLOR, i));
            transitions.add(new PauseTransition(Duration.millis(AnimationController.PAUSEDELAY)));
            transitions.add(takeback(arr, text_arr,4,9));
        }
//        

        if (left < n && arr[max].getValue() < arr[left].getValue()) {
            showDescription(d[1]);
//            transitions.add(colorCNode(arr, SELECT_COLOR, left, max));
//            transitions.add(colorCNode(arr, START_COLOR, ROOT_COLOR, left, max));
            max = left;
            transitions.add(colorCNode(arr, SELECT_COLOR, max));
            transitions.add(colorCNode(arr, START_COLOR, max));
            transitions.add(colorCNode(arr, SELECT_COLOR, max));
            transitions.add(colorCNode(arr, START_COLOR, max));
                   }

        if (right < n && arr[max].getValue() < arr[right].getValue()) {
//            transitions.add(colorCNode(arr, SELECT_COLOR, right, max));
//            transitions.add(colorCNode(arr, START_COLOR, ROOT_COLOR, right, max));
            showDescription(d[2]);
            max = right;
            transitions.add(colorCNode(arr, SELECT_COLOR, max));
            transitions.add(colorCNode(arr, START_COLOR, max));
            transitions.add(colorCNode(arr, SELECT_COLOR, max));
            transitions.add(colorCNode(arr, START_COLOR, max));
                   }

        if (max != i) {
            showDescription(d[3]);
            transitions.add(swap(arr, text_arr, i, max));
            transitions.add(colorCNode(arr, START_COLOR, max));
            heapify(arr, text_arr, max, n, d);
        }
        transitions.add(colorCNode(arr, START_COLOR, max));

    }

    private void heapSort(CNode[] arr, TNode[] text_arr, TNode[] d) {
        // build initial max heap
        showDescription(d[6]);
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            heapify(arr, text_arr, i, arr.length, d);
        }

        // swap root node with final elt, heapify subarray
        showDescription(d[7]);
        for (int i = arr.length - 1; i > 0; i--) {

            transitions.add(colorCNode(arr, ROOT_COLOR, 0));
            showDescription(d[4]);
            transitions.add(swap(arr, text_arr, 0, i));
            transitions.add(new PauseTransition(Duration.millis(500)));
            transitions.add(colorCNode(arr, START_COLOR, i));
            showDescription(d[5]);
            transitions.add(colorCNode(selectSubTree(arr, i), SELECT_COLOR));
            transitions.add(colorCNode(selectSubTree(arr, i), START_COLOR));
            transitions.add(colorCNode(selectSubTree(arr, i), SELECT_COLOR));
            transitions.add(colorCNode(selectSubTree(arr, i), START_COLOR));

            heapify(arr, text_arr, 0, i, d);
        }
    }

    private ArrayList<CNode> selectSubTree(CNode[] arr, int n) {
        ArrayList<CNode> list = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            list.add(arr[i]);
        }

        return list;
    }

    @Override
    public ArrayList<Transition> startSort(CNode[] arr, TNode[] text_arr, TNode[] d) {
        transitions.clear();
        d[0].setText("largest = root i\nleft = 2*i + 1 | right = 2*i + 2");
        d[1].setText("left > root -> largest = left");
        d[2].setText("right > root -> largest = right");
        d[3].setText("root != largest -> swap root,largest");
        d[4].setText("Move current root to end");
        d[5].setText("call max heapify on the reduced heap");
        d[6].setText("Build max heap\nHeapify from i = 10/2 - 1");
        d[7].setText("One by one extract an element from heap");

        for (int i = 0; i <= 7; i++) {
            d[i].setX((AnimationController.WINDOW_WIDTH-200-d[i].getBoundsInLocal().getWidth())/2);
            d[i].setY(410);
        }
        heapSort(arr, text_arr, d);

        transitions.add(colorCNode(Arrays.asList(arr), SORTED_COLOR));
        return transitions;
    }

}
