package GUI;

import CNode.CNode;
import CNode.RandomCNodes;
import CNode.TNode;
import SortingAlgorithms.AbstractSort;
import SortingAlgorithms.BubbleSort;
import SortingAlgorithms.HeapSort;
import SortingAlgorithms.RadixSort;
import SortingAlgorithms.QuickSort;
import SortingAlgorithms.RadixSort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.animation.SequentialTransition;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Scale;
import javafx.util.StringConverter;

public class AnimationController extends BorderPane {

    public static final int WINDOW_WIDTH = 900;
    public static final int WINDOW_HEIGHT = 500;
    public static final int XGAP = 2;
    public static final int BUTTONROW_BOUNDARY = 80;
    public static int NO_OF_DESCRIPTION = 10;
    public static int NO_OF_CNODES = 10;
    public static int DELAY = 450;
    public static int PAUSEDELAY = 1000;
    private static AbstractSort abstractSort;

    private Pane display;
    private HBox optionsPanel;

    private Button sortButton;
    private Button randomButton;
    private Button pauseButton;
    private Button continueButton;
    private ChoiceBox<String> delayChoiceBox;
    private ChoiceBox<AbstractSort> choiceBox;
    private CNode[] cnodes;
    private TNode[] tnodes;
    StackPane left = new StackPane();
    StackPane right = new StackPane();
    StackPane top = new StackPane();
    private TNode[] index;
    private TNode[] description;

    public AnimationController() {
        this.description = RandomCNodes.setDescription(NO_OF_DESCRIPTION);
        this.cnodes = RandomCNodes.randomCNodes(NO_OF_CNODES);
        this.setStyle("-fx-background-color: #1e2023");
        
//      OPTION PANEL
        this.optionsPanel = new HBox();
        this.optionsPanel.setAlignment(Pos.CENTER);
        this.optionsPanel.setPrefHeight(BUTTONROW_BOUNDARY);
        this.optionsPanel.setSpacing(10);
        this.display = new Pane();
        left.setPrefWidth(100);
        right.setPrefWidth(100);
        top.setPrefHeight(80);
        this.setLeft(left);
        this.setRight(right);
        this.setTop(top);
        this.setCenter(display);
        this.setBottom(optionsPanel);
        

        this.sortButton = new Button("Sort");
        this.sortButton.setPrefWidth(100);
        this.randomButton = new Button("Generate");
        this.randomButton.setPrefWidth(100);
        this.pauseButton = new Button("| |");
        this.pauseButton.setPrefWidth(50);
        this.continueButton = new Button("▶");
        this.continueButton.setPrefWidth(50);
        this.pauseButton.setDisable(true);
        this.continueButton.setDisable(true);
        this.choiceBox = new ChoiceBox<>();
        this.choiceBox.setPrefWidth(100);

        this.delayChoiceBox = new ChoiceBox<>();
        this.delayChoiceBox.setPrefWidth(100);

        optionsPanel.getChildren().add(delayChoiceBox);
        optionsPanel.getChildren().add(choiceBox);
        optionsPanel.getChildren().add(randomButton);
        optionsPanel.getChildren().add(sortButton);   
        optionsPanel.getChildren().add(pauseButton);
        optionsPanel.getChildren().add(continueButton);
        
        List<String> speed = new ArrayList<>();
        speed.add("Fast");
        speed.add("Medium");
        speed.add("Slow");
        
//      SPEED OPTIONS
        delayChoiceBox.setItems(FXCollections.observableArrayList(speed));
        delayChoiceBox.getSelectionModel().select(1);
        this.cnodes = RandomCNodes.randomCNodes(NO_OF_CNODES);
        this.tnodes = RandomCNodes.getTnodes(cnodes);
        this.index = RandomCNodes.setIndex(cnodes);

        List<AbstractSort> abstractSortList = new ArrayList<>();
        abstractSortList.add(new BubbleSort());
        abstractSortList.add(new QuickSort());
        abstractSortList.add(new HeapSort());
        abstractSortList.add(new RadixSort());
        display.getChildren().addAll(Arrays.asList(cnodes));
        display.getChildren().addAll(Arrays.asList(tnodes));
        display.getChildren().addAll(Arrays.asList(index));
        display.getChildren().addAll(Arrays.asList(description));

//      SORT BUTTON ********************************************************
        sortButton.setOnAction(event -> {
            sortButton.setDisable(true);
            randomButton.setDisable(true);
            delayChoiceBox.setDisable(true);
            choiceBox.setDisable(true);
            
            pauseButton.setDisable(false);
            
            abstractSort = choiceBox.getSelectionModel().getSelectedItem();
            if (delayChoiceBox.getSelectionModel().getSelectedItem() == "Medium") {
                DELAY = 300;
                PAUSEDELAY = 800;
            } else if (delayChoiceBox.getSelectionModel().getSelectedItem() == "Fast") {
                DELAY = 100;
                PAUSEDELAY = 300;
            } else {
                DELAY = 450;
                PAUSEDELAY = 1000;
            }
            SequentialTransition sq = new SequentialTransition();

            sq.getChildren().addAll(abstractSort.startSort(cnodes, tnodes, description));

            sq.setOnFinished(e -> {
                randomButton.setDisable(false);
                choiceBox.setDisable(false);
                delayChoiceBox.setDisable(false);
                pauseButton.setDisable(true);
            });
            
            
            sq.play();
            pauseButton.setOnAction(e ->{
                pauseButton.setDisable(true);
                continueButton.setDisable(false);
                sq.pause();
            });
            continueButton.setOnAction(e ->{
                pauseButton.setDisable(false);
                continueButton.setDisable(true);
                sq.play();
            });
        });
  
//      RANDOM BUTTON ********************************************************
        randomButton.setOnAction(event -> {
            sortButton.setDisable(false);
            display.getChildren().clear();
            cnodes = RandomCNodes.randomCNodes(NO_OF_CNODES);
            tnodes = RandomCNodes.getTnodes(cnodes);
            description = RandomCNodes.setDescription(NO_OF_DESCRIPTION);
            display.getChildren().addAll(Arrays.asList(cnodes));
            display.getChildren().addAll(Arrays.asList(tnodes));
            display.getChildren().addAll(Arrays.asList(index));
            display.getChildren().addAll(Arrays.asList(description));

        });

//      CHOICEBOX OPTIONS ********************************************************
        choiceBox.setItems(FXCollections.observableArrayList(abstractSortList));
        choiceBox.getSelectionModel().select(3);
        choiceBox.setConverter(new StringConverter<AbstractSort>() {
            @Override
            public String toString(AbstractSort abstractSort) {
                if (abstractSort == null) {
                    return "";
                } else {
                    return abstractSort.getClass().getSimpleName();
                }
            }

            @Override
            public AbstractSort fromString(String s) {
                return null;
            }
        });

    }

}
