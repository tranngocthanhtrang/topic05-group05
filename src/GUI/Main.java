package GUI;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {
	
	private Button okButton;
	private Label NO_OF_CNODES_Lbl;
	private Scene scene, startScene;
	private AnimationController animationController;


	@Override
	public void start(Stage stage) {
                animationController = new AnimationController();
                
                scene = new Scene(animationController, AnimationController.WINDOW_WIDTH, AnimationController.WINDOW_HEIGHT);
                scene.getStylesheets().add("GUI/SortingVisualizerStylesheet.css");
		stage.setResizable(false);
		stage.setX(400);
                
		stage.setTitle("Sorting Algorithms App");
                stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
